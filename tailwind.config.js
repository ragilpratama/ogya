const colors = require("tailwindcss/colors");

module.exports = {
  important: true,
//   content: ["./src/**/*.{js,jsx,ts,tsx}"],
  content: ["./src/**/*.{html,js}", './src/components/**/*.{js,jsx,ts,tsx}'],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        "primary-light": "#F7F8FC",
        "secondary-light": "#FFFFFF",
        "ternary-light": "#f6f7f8",

        "primary-dark": "#0D2438",
        "secondary-dark": "#102D44",
        "ternary-dark": "#1E3851",
      },
      container: {
        padding: {
          DEFAULT: "1rem",
          sm: "2rem",
          lg: "5rem",
          xl: "6rem",
          "2xl": "8rem",
        },
      },
      fontSize: {
        sm: "0.8rem",
        base: "1rem",
        xl: "1.25rem",
        lg: "6rem",
        "2xl": "1.563rem",
        "3xl": "1.953rem",
        "4xl": [ '6rem', { lineHeight: '6rem' } ],
      },
    },
  },
  variants: {
    extend: { opacity: ["disabled"] },
  },
  plugins: ["@tailwindcss/forms"],
};
