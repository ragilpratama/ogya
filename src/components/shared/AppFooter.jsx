import {
  FiGithub,
  FiTwitter,
  FiLinkedin,
  FiGlobe,
  FiYoutube,
} from "react-icons/fi";
import AppFooterCopyright from "./AppFooterCopyright";

const socialLinks = [
  {
    id: 1,
    icon: <FiGlobe />,
    url: "https://www.stoman.me/",
  },
  {
    id: 2,
    icon: <FiGithub />,
    url: "https://github.com/realstoman",
  },
  {
    id: 3,
    icon: <FiTwitter />,
    url: "https://twitter.com/realstoman",
  },
  {
    id: 4,
    icon: <FiLinkedin />,
    url: "https://www.linkedin.com/in/realstoman",
  },
  {
    id: 5,
    icon: <FiYoutube />,
    url: "https://www.youtube.com/c/realstoman",
  },
];

const AppFooter = () => {
  return (
    <div className="container mx-auto">
      <div className="pt-20 sm:pt-30 pb-8 mt-5">
        {/* Footer social links */}
        <div className="flex justify-between mb-20">
          <div className="flex flex-col md:gap-4 md:flex-row">
            <div className="flex flex-col mt-2 md:mt-2">
              <p className="font-general-bold text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                About Us
              </p>
              <div className="flex flex-col">
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Vision
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Mission
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Values
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Our Leaders
                </p>
              </div>
            </div>
            <div className="flex flex-col mt-2 md:mt-2">
              <p className="font-general-bold text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                Services
              </p>
              <div className="flex flex-col">
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Our Services
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Technology Partners
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Portfolio
                </p>
              </div>
            </div>
            <div className="flex flex-col mt-2 md:mt-2">
              <p className="font-general-bold text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                Investor Relations
              </p>
              <div className="flex flex-col">
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Financial Reports
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  CSR
                </p>
              </div>
            </div>
            <div className="flex flex-col mt-2 md:mt-2">
              <p className="font-general-bold text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                Careers
              </p>
              <div className="flex flex-col">
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Life at Ogya
                </p>
                <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-left">
                  Jobs
                </p>
              </div>
            </div>
          </div>
          <div className="flex flex-col mt-2 md:mt-2">
            <p className="text-sm text-primary-dark dark:text-primary-light mb-1 md:mb-2 text-right">
              Privacy Policy
            </p>
            <p className="text-sm text-primary-dark dark:text-primary-light mb-4 text-right">
              Terms of Service
            </p>
            <ul className="flex gap-4">
              {socialLinks.map((link) => (
                <a
                  href={link.url}
                  target="__blank"
                  key={link.id}
                  className="text-gray-400 hover:text-indigo-500 dark:hover:text-indigo-400 cursor-pointer rounded-lg bg-gray-50 dark:bg-ternary-dark hover:bg-gray-100 shadow-sm p-1 duration-300"
                >
                  <i className="text-xl">{link.icon}</i>
                </a>
              ))}
            </ul>
          </div>
        </div>
        <AppFooterCopyright />
      </div>
    </div>
  );
};

export default AppFooter;
