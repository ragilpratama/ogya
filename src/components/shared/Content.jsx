import useThemeSwitcher from '../../hooks/useThemeSwitcher';
import { FiArrowDownCircle } from 'react-icons/fi';
import developerLight from '../../images/developer.svg';
import developerDark from '../../images/developer-dark.svg';
import { motion } from 'framer-motion';
import ImageSlider from '../../ImageSlider';
// import Button from '../reusable/Button';
import img1 from   '../../images/brands/fila_color.png';
import img2 from '../../images/brands/adidas_color.png';
import img3 from   '../../images/brands/nb_color.png';
import img4 from   '../../images/brands/puma_color.png';

const Content = () => {
    const [activeTheme] = useThemeSwitcher();

    const slides = [
        { url: img1, title: "FILA", message: "Ogya's services is way too good for the price! They shown major expertise throughout the project" },
        { url: img2, title: "ADIDAS", message: "Best quality design" },
        { url: img3, title: "NEW BALANCE", message: "Fast & Furious" },
        { url: img4, title: "PUMA", message: "Full of professional developer, recommended" }
    ];
    const containerStyles = {
        height: "350px",
        margin: "0 auto",
    };

    return (
        <motion.section
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ ease: 'easeInOut', duration: 0.9, delay: 0.2 }}
            className="flex flex-col sm:justify-between items-center sm:flex-row mt-15 mb-10"
        >
            <div className="flex flex-col w-full md:w-8/12 text-right">
                <div style={containerStyles}>
                    <ImageSlider slides={slides} />
                </div>
            </div>
            <div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.2,
                    }}
                    className="font-general-bold text-3xl lg:text-8xl xl:text-8xl text-right sm:text-right text-ternary-dark dark:text-primary-light mt-20 md:mt-0"
                >
                    Satisfied collaborators
                </motion.div>
                <motion.p
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.2,
                    }}
                    className="font-general-medium mt-4 text-lg md:text-xl lg:text-2xl xl:text-3xl text-right sm:text-right text-gray-500 dark:text-gray-200"
                >
                    Hear their stories to find out more.
                </motion.p>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.3,
                    }}
                    className="flex justify-right float-right sm:block"
                >
                    <a
                        // download="Stoman-Resume.pdf"
                        href=""
                        className="font-general-medium flex justify-right items-center w-36 sm:w-48 mt-12 mb-6 sm:mb-0 text-lg justify-right"
                        aria-label="Download Resume"
                    >
                        <span className="block bg-blue rounded-xl text-center text-lg w-40 text-white dark:text-ternary-light hover:text-white dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 sm:px-4">
                            Contact Us
                        </span>
                    </a>
                </motion.div>
            </div>
        </motion.section>
    );
};

export default Content;
