import { useState } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { FiMenu, FiMoon, FiSun, FiX } from "react-icons/fi";
import useThemeSwitcher from "../../hooks/useThemeSwitcher";
import HireMeModal from "../HireMeModal";
import logoLight from "../../images/logo.png";
import logoDark from "../../images/logo.png";
import { motion } from "framer-motion";
import Dropdown from '../../Dropdown';
import DropdownServices from '../../DrowpdownServices';
// import Button from "../reusable/Button";

import flagId from "../../images/id.png";
import flagEn from "../../images/en.png";
import DropdownInvestor from "../../DropdownInvestor";
import DropdownCareers from "../../DropdownCareers";

const AppHeader = () => {
  const { i18n, t } = useTranslation();
  const [showMenu, setShowMenu] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [lang, setLang] = useState("en");
  const [activeTheme, setTheme] = useThemeSwitcher();
  const [dropdown, setDropdown] = useState(false);
  const [dropdownService, setDropdownServices] = useState(false);
  const [dropdownInvestor, setDropdownInvestor] = useState(false);
  const [dropdownCareers, setDropdownCareers] = useState(false);
  const [navbar, setNavbar] = useState();

  function toggleMenu() {
    if (!showMenu) {
      setShowMenu(true);
    } else {
      setShowMenu(false);
    }
  }

  function showHireMeModal() {
    if (!showModal) {
      document
        .getElementsByTagName("html")[0]
        .classList.add("overflow-y-hidden");
      setShowModal(true);
    } else {
      document
        .getElementsByTagName("html")[0]
        .classList.remove("overflow-y-hidden");
      setShowModal(false);
    }
  }

  const onChangeLang = () => {
    if (lang === "id") {
      setLang("en");
      i18n.changeLanguage("en");
    } else {
      setLang("id");
      i18n.changeLanguage("id");
    }
  };


  return (
    <motion.nav
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      id="nav"
      className="sm:container sm:mx-auto"
    >
      <div className="z-10 max-w-screen-lg xl:max-w-screen-xl block sm:flex sm:justify-between sm:items-center py-6">
        {/* Header menu links and small screen hamburger menu */}
        <div className="flex justify-between items-center px-4 sm:px-0">
          <div>
            <Link to="/">
              {activeTheme === "dark" ? (
                <img src={logoDark} className="w-24" alt="Dark Logo" />
              ) : (
                <img src={logoLight} className="w-24" alt="Light Logo" />
              )}
            </Link>
          </div>

          {/* Theme switcher small screen */}
          <div
            onClick={() => setTheme(activeTheme)}
            aria-label="Theme Switcher"
            className="block sm:hidden ml-0 bg-primary-light dark:bg-ternary-dark p-3 shadow-sm rounded-xl cursor-pointer"
          >
            {activeTheme === "dark" ? (
              <FiMoon className="text-ternary-dark hover:text-gray-400 dark:text-ternary-light dark:hover:text-primary-light text-xl" />
            ) : (
              <FiSun className="text-gray-200 hover:text-gray-50 text-xl" />
            )}
          </div>

          {/* Small screen hamburger menu */}
          <div className="sm:hidden">
            <button
              onClick={toggleMenu}
              type="button"
              className="focus:outline-none"
              aria-label="Hamburger Menu"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                className="h-7 w-7 fill-current text-secondary-dark dark:text-ternary-light"
              >
                {showMenu ? (
                  <FiX className="text-3xl" />
                ) : (
                  <FiMenu className="text-3xl" />
                )}
              </svg>
            </button>
          </div>
        </div>

        {/* Header links small screen */}
        <div
          className={
            showMenu
              ? "block m-0 sm:ml-4 mt-5 sm:mt-3 sm:flex p-5 sm:p-0 justify-center items-center shadow-lg sm:shadow-none"
              : "hidden"
          }
        >
          {/* <Link
            to="/projects"
            className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2"
            aria-label="Projects"
          >
            {t("home")}
          </Link> */}
          <div
            className='nav-item'
            onMouseEnter={() => setDropdown(true)}
            onMouseLeave={() => setDropdown(false)}
          >
            <Link
              to="/about"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("about")}
            </Link>
            {dropdown && <Dropdown />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownServices(true)}
            onMouseLeave={() => setDropdownServices(false)}
          >
            <Link
              to="/service"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("service")}
            </Link>
            {dropdownService && <DropdownServices />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownInvestor(true)}
            onMouseLeave={() => setDropdownInvestor(false)}
          >
            <Link
              to="/investor"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("investor")}
            </Link>
            {dropdownInvestor && <DropdownInvestor />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownCareers(true)}
            onMouseLeave={() => setDropdownCareers(false)}
          >
            <Link
              to="/careers"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("careers")}
            </Link>
            {dropdownCareers && <DropdownCareers />}
          </div>
          <Link
            to="/contact"
            className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
            aria-label="Contact"
          >
            {t("contact")}
          </Link>
          {/* <div className="border-t-2 pt-3 sm:pt-0 sm:border-t-0 border-primary-light dark:border-secondary-dark">
            <span
              onClick={showHireMeModal}
              className="font-general-medium sm:hidden block text-left text-md bg-indigo-500 hover:bg-indigo-600 text-white shadow-sm rounded-sm px-4 py-2 mt-2 duration-300 w-24"
              aria-label="Hire Me Button"
            >
              <Button title="Hire Me" />
            </span>
          </div> */}
        </div>

        {/* Header links large screen */}
        <div className="font-general-medium hidden m-0 sm:ml-4 sm:flex p-5 sm:p-0 justify-right shadow-lg sm:shadow-none">
          <div
            className='nav-item'
            onMouseEnter={() => setDropdown(true)}
            onMouseLeave={() => setDropdown(false)}
          >
            <Link
              to="/about"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("about")}
            </Link>
            {dropdown && <Dropdown />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownServices(true)}
            onMouseLeave={() => setDropdownServices(false)}
          >
            <Link
              to="/service"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("service")}
            </Link>
            {dropdownService && <DropdownServices />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownInvestor(true)}
            onMouseLeave={() => setDropdownInvestor(false)}
          >
            <Link
              to="/investor"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("investor")}
            </Link>
            {dropdownInvestor && <DropdownInvestor />}
          </div>
          <div
            className='nav-item'
            onMouseEnter={() => setDropdownCareers(true)}
            onMouseLeave={() => setDropdownCareers(false)}
          >
            <Link
              to="/careers"
              className="block text-left text-lg text-primary-dark dark:text-ternary-light hover:text-secondary-dark dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 border-t-2 pt-3 sm:pt-2 sm:border-t-0 border-primary-light dark:border-secondary-dark"
              aria-label="About Me"
            >
              {t("careers")}
            </Link>
            {dropdownCareers && <DropdownCareers />}
          </div>
          <Link
            to="/contact"
            className="block bg-blue rounded-xl text-left text-lg text-white dark:text-ternary-light hover:text-white dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 sm:px-4"
            aria-label="Contact"
          >
            {t("contact")}
          </Link>
        </div>

        {/* Header right section buttons */}
        <div className="hidden sm:flex justify-between items-center flex-col md:flex-row">
          {/* Theme switcher large screen */}
          <div
            onClick={() => onChangeLang()}
            className="bg-primary-light dark:bg-ternary-dark shadow-sm rounded-sm cursor-pointer"
            style={{ padding: 3.5 }}
          >
            <img src={lang === "id" ? flagId : flagEn} alt="" width={36} />
          </div>
          <div
            onClick={() => setTheme(activeTheme)}
            aria-label="Theme Switcher"
            className="ml-8 bg-primary-light dark:bg-ternary-dark p-2 shadow-sm rounded-xl cursor-pointer"
          >
            {activeTheme === "dark" ? (
              <FiMoon className="text-ternary-dark hover:text-gray-400 dark:text-ternary-light dark:hover:text-primary-light text-xl" />
            ) : (
              <FiSun className="text-gray-200 hover:text-gray-50 text-xl" />
            )}
          </div>
        </div>
      </div>
      {/* Hire me modal */}
      <div>
        {showModal ? (
          <HireMeModal onClose={showHireMeModal} onRequest={showHireMeModal} />
        ) : null}
        {showModal ? showHireMeModal : null}
      </div>
    </motion.nav>
  );
};

export default AppHeader;
