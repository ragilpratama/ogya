import i18n from "i18next";
import { initReactI18next } from "react-i18next";

i18n
//   .use(i18nBackend)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    // lng: getCurrentLang(),
    lng: "en",
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        translation: {
          title: "Multi-language app",
          about: "About Us",
          service: "Services",
          investor: "Investor Relations",
          careers: "Careers",
          contact: "Contact Us",
          // about: "About OGYA",
          // home: "Home",
        },
      },
      id: {
        translation: {
          title: "Aplicación en varios idiomas",
          about: "Tentang Kami",
          service: "Layanan",
          investor: "Hubungan Investor",
          careers: "Karir",
          contact: "Hubungi Kami",
          // about: "Tentang OGYA",
          // home: "Beranda",
        },
      },
    },
  });

export default i18n;