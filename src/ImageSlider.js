import { useState } from "react";
import star from "./images/5star.png";

const slideStyles = {
  width: "100%",
  height: "100%",
  borderRadius: "10px",
  backgroundColor: "#f7f8fc",
  boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
  // backgroundSize: "cover",
  // backgroundPosition: "center",
};

const rightArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  right: "10px",
  fontSize: "45px",
  color: "#333",
  zIndex: 1,
  fontSize: "24px",
  cursor: "pointer",
};

const leftArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  left: "10px",
  fontSize: "45px",
  color: "#333",
  zIndex: 1,
  fontSize: "24px",
  cursor: "pointer",
};

const sliderStyles = {
  position: "relative",
  height: "100%",
  width: "260px",
};

const dotsContainerStyles = {
  display: "flex",
  justifyContent: "center",
};

const dotStyle = {
  margin: "0 3px",
  cursor: "pointer",
  fontSize: "20px",
};

const ImageSlider = ({ slides }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const goToPrevious = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };
  const goToNext = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };
  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
  };
  const slideStylesWidthBackground = {
    ...slideStyles,
    display: "flex",
    justifyContent: "center",
    marginTop: 20,
    padding: 20,
    // backgroundImage: `url(${slides[currentIndex].url})`,
  };

  return (
    <div style={sliderStyles}>
      <div>
        <div onClick={goToPrevious} style={leftArrowStyles}>
          ❰
        </div>
        <div onClick={goToNext} style={rightArrowStyles}>
          ❱
        </div>
      </div>
      <div style={slideStylesWidthBackground} className="flex flex-col">
        <div className="flex justify-center rounded-xl bg-dark">
          <img src={slides[currentIndex].url} alt="star" width={125} className="rounded-xl"/>
        </div>
        <div className="font-general-bold flex justify-center mb-1 mt-10">
          {slides[currentIndex].title}
        </div>
        <div className="flex text-center justify-center mb-5">
          <img src={star} alt="star" width={125} />
        </div>
        <div className="flex text-center justify-center">“{slides[currentIndex].message}”</div>
      </div>
      <div style={dotsContainerStyles}>
        {slides.map((slide, slideIndex) => (
          <div
            style={dotStyle}
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
            className="text-ternary-dark dark:text-primary-light"
          >
            ●
          </div>
        ))}
      </div>
    </div>
  );
};

export default ImageSlider;
