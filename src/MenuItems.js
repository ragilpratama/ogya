export const AboutUs = [
    {
        title: 'Vision',
        path: '/vision',
        cName: 'dropdown-link'
    },
    {
        title: 'Mission',
        path: '/mission',
        cName: 'dropdown-link'
    },
    {
        title: 'Values',
        path: '/values',
        cName: 'dropdown-link'
    },
    {
        title: 'Our Leaders',
        path: '/ourLeaders',
        cName: 'dropdown-link'
    }
];

export const Services = [
    {
        title: 'Our Services',
        path: '/ourServices',
        cName: 'dropdown-link'
    },
    {
        title: 'Technology Partners',
        path: '/partners',
        cName: 'dropdown-link'
    },
    {
        title: 'Portfolio',
        path: '/design',
        cName: 'dropdown-link'
    }
];

export const Investor = [
    {
        title: 'Financial Reports',
        path: '/financialReports',
        cName: 'dropdown-link'
    },
    {
        title: 'CSR',
        path: '/consulting',
        cName: 'dropdown-link'
    }
];

export const Careers = [
    {
        title: 'Life at Ogya',
        path: '/marketing',
        cName: 'dropdown-link'
    },
    {
        title: 'Jobs',
        path: '/jobs',
        cName: 'dropdown-link'
    }
];