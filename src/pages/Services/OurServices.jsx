import '../../css/style.css';

const OurServices = () => {
    return (
        <>
            <div className="container mx-auto">
                <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                    <div className="text-center">
                        <p className="font-general-medium mt-4 text-lg md:text-xl lg:text-2xl xl:text-3xl text-center sm:text-center text-gray-500 dark:text-gray-200">
                            Find out about our Solutions
                        </p>
                    </div>
                </section>
            </div>
            <div class="card-category-1">
                <div class="basic-card basic-card-aqua rounded-xl">
                    <div class="card-content">
                        <span class="card-title">Custom Software Development Services</span>

                    </div>
                    <div class="card-link">
                        <a href="#" title="Read Full"><span>Read Full</span></a>
                    </div>
                </div>
                <div class="basic-card basic-card-aqua rounded-xl">
                    <div class="card-content mt-10">
                        <span class="card-title">Middleware/Integration Services</span>

                    </div>

                    <div class="card-link">
                        <a href="#" title="Read Full"><span>Read Full</span></a>
                    </div>
                </div>
                <div class="basic-card basic-card-aqua rounded-xl">
                    <div class="card-content mt-10">
                        <span class="card-title">Data Warehouse and Business Intelligence</span>
                    </div>

                    <div class="card-link">
                        <a href="#" title="Read Full"><span>Read Full</span></a>
                    </div>
                </div>

                <div class="basic-card basic-card-aqua rounded-xl">
                    <div class="card-content mt-15">
                        <span class="card-title">Managed Services</span>
                    </div>

                    <div class="card-link">
                        <a href="#" title="Read Full"><span>Read Full</span></a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default OurServices;
