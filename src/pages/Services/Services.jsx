import useThemeSwitcher from '../../hooks/useThemeSwitcher';
import { FiArrowDownCircle } from 'react-icons/fi';
import developerLight from '../../images/developer.svg';
import developerDark from '../../images/developer-dark.svg';
import { motion } from 'framer-motion';
import Content from '../../components/shared/Content';
import OurServices from '../Services/OurServices';
import Partners from '../Services/Partners';
// import Button from '../reusable/Button';

const Services = () => {
    const [activeTheme] = useThemeSwitcher();

    return (
        <motion.section
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ ease: 'easeInOut', duration: 0.9, delay: 0.2 }}
            className="flex flex-col sm:justify-between items-center sm:flex-row mt-15 mb-10"
        >
            <div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.2,
                    }}
                    className="font-general-bold text-3xl lg:text-8xl xl:text-8xl text-right sm:text-right text-ternary-dark dark:text-primary-light mt-20 md:mt-0"
                >
                    Custom-made solutions, just for you.
                </motion.div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.3,
                    }}
                    className="flex justify-right float-right sm:block"
                >
                    <a
                        // download="Stoman-Resume.pdf"
                        href=""
                        className="font-general-medium flex justify-right items-center w-36 sm:w-48 mt-12 mb-6 sm:mb-0 text-lg justify-right"
                        aria-label="Download Resume"
                    >
                        <span className="block bg-blue rounded-xl text-center text-lg w-40 text-white dark:text-ternary-light hover:text-white dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 sm:px-4">
                            Get a quote
                        </span>
                    </a>
                </motion.div>
                <OurServices></OurServices>
                <Partners></Partners>

            </div>
        </motion.section>

    );
};

export default Services;
