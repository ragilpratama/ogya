const FinancialReport = () => {
    return (
        <>
            <div>
                <div className="container mx-auto">
                    <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                        <div className="text-center">
                            <p className="font-general-medium mt-8 text-lg md:text-xl lg:text-2xl xl:text-3xl text-center sm:text-center text-gray-500 dark:text-gray-200">
                                2022 Financial Highlights
                            </p>
                        </div>
                    </section>
                </div>
                <div className="container mx-auto">
                    <div className="flex justify-center">
                        <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                            <p className="font-general-bold text-xl text-center">
                                16.2 %
                            </p>
                            <p className="text-xl text-center mt-5">
                                Return on Stockholder's Equity
                            </p>
                        </div>

                        <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                            <p className="font-general-bold text-xl text-center">
                                $9.6B
                            </p>
                            <p className="text-xl text-center mt-5">
                                Total Revenues
                            </p>
                        </div>
                        <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                            <p className="font-general-bold text-xl text-center">
                                $3.66B
                            </p>
                            <p className="text-xl text-center mt-5">
                                Net Income Per Diluted Share
                            </p>
                        </div>

                    </div>
                </div>

                <div className="flex justify-center">
                    <button style={button}>Download 2022 Annual Report</button>
                    <button style={button}>Download 2022 Annual Report</button>
                </div>

            </div>
        </>
    );
};

export default FinancialReport;

const insideStyles = {
    background: "#e5e7eb52",
    width: "260px",
    padding: "30px",
    margin: "10px",
    left: "50%",
    color: "white",
};

const button = {
    background: "#ddd",
    border: "noun",
    color: "black",
    padding: "10px 20px ",
    textAlign: "center",
    textDecoration: "none",
    display: "inline-block",
    margin: "4px 2px",
    curson: "pointer",
    borderRadius: "16px"
};


