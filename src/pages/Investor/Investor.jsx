import FinancialReport from '../Investor/FinancialReport';
const Investor = () => {
    return (
        <>
            <div className="container mx-auto">
                <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                    <div className="text-center">
                        <p className="font-general-bold text-3xl lg:text-8xl xl:text-8xl text-center sm:text-center text-ternary-dark dark:text-primary-light">
                            Investor Relations
                        </p>
                        <p className="font-general-bold text-center text-ternary-dark dark:text-primary-light">
                            We can achieve big things because of your constant support.
                            Here is our part of showing up.
                        </p>
                        <p style={text} className="font-general-bold text-center text-ternary-dark dark:text-primary-light mt-10 ">
                            Read more
                        </p>
                    </div>
                </section>
            </div>
            <FinancialReport />
        </>
    );
};

export default Investor;

const text = {
    textDecoration: "underline",
}