import blue from "../../images/blue.png";
import svg6 from "../../images/Group6.svg";
import svg5 from "../../images/Group5.svg";
import svg4 from "../../images/Group4.svg";


const OurValues = () => {
    return (
        <>
            <div className="container mx-auto">
                <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                    <div className="text-center">
                        <p className="font-general-medium mt-8 text-lg md:text-xl lg:text-2xl xl:text-3xl text-center sm:text-center text-gray-500 dark:text-gray-200">
                            Our Values
                        </p>
                    </div>
                </section>
            </div>
            <div className="flex justify-center">
                <div style={container}>
                    <img alt="image1" src={svg4} style={styleImage} />
                </div>
                <div style={container}>
                    <img alt="image1" src={svg5} style={styleImage} />
                </div>
                <div style={container}>
                    <img alt="image1" src={svg6} style={styleImage} />
                </div>
            </div>
        </>
    );
};

export default OurValues;

const styleImage = {
    borderRadius: "50%",
    margin: "10px",
    width: "350px",
    padding: "30px",
};

const styleText = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
}

const container = {
    position: "relative",
    textAlign: "center",
    color: "white",
}