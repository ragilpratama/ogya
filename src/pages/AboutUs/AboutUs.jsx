import Section from '../AboutUs/Section';
import Vision from '../AboutUs/Vision';
import Mission from '../AboutUs/Mission';
import OurValues from '../AboutUs/OurValues';
import OurLeaders from '../AboutUs/OurLeaders';

const AboutUs = () => {
    return (
        <>
            <div className="container mx-auto">
                {/* <AppBanner></AppBanner> */}
                <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                    <div className="text-center">
                        <p className="font-general-bold text-3xl lg:text-8xl xl:text-8xl text-center sm:text-center text-ternary-dark dark:text-primary-light">
                            We have a dream to help you transform.
                        </p>
                        {/* <div className="flex justify-center">
                            <img alt="Powered By" src={ImgPoweredBy} width={370} />
                        </div> */}
                    </div>
                </section>
            </div>
            <Section />
            <div>
                <OurLeaders />
            </div>
            <div>
                <Vision />
            </div>
            <div>
                <Mission />
            </div>
            <div>
                <OurValues />
            </div>
        </>
    );
};

export default AboutUs;