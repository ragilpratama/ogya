const Section = () => {
    return (
        <>
            <div className="container mx-auto">
                <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                    <div className="text-center">
                        <p className="font-general-bold text-2xl lg:text-4xl xl:text-6xl text-left sm:text-center text-ternary-dark dark:text-primary-light">
                            Our Journey
                        </p>
                        <p className="font-general-medium mt-4 text-lg md:text-xl text-left sm:text-left text-gray-500 dark:text-gray-200">
                            Founded in 2017, Ogya started with the dream of helping other businesses improve.
                            With the knowledge and experience to improve business processes through digitalization,
                            Ogyan managed to help organizations thrive.
                        </p>
                        <p className="font-general-medium mt-4 text-lg md:text-xl text-left sm:text-left text-gray-500 dark:text-gray-200">
                            Acting as a catalyst, Ogya has helped numerous businesses grow until now.
                            We are continuously working towards our dream, now more specifically to help
                            those in the financial industries.
                        </p>
                        <p className="font-general-medium mt-4 text-lg md:text-xl text-left sm:text-left text-gray-500 dark:text-gray-200">
                            We believe through digitalization, we can revolutionize the industry.
                            Hence making impactful changes for the economy.
                        </p>
                    </div>
                </section>
            </div>
        </>
    );
};

export default Section;
