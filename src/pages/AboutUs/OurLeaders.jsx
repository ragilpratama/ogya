import '../../css/style.css';
import image1 from '../../images/profile.jpeg';

const OurLeaders = () => {
    return (
        <>

            <div className="container mx-auto">
                <div className="flex flex-col sm:justify-between items-center sm:flex-row mt-15 mb-8">
                    <section>
                        <p className="font-general-medium text-1xl lg:text-4xl xl:text-8xl text-left sm:text-center text-ternary-dark dark:text-primary-light font-size:xx-large">
                            Met Our
                        </p>
                        <p className="font-general-bold text-2xl lg:text-4xl xl:text-8xl text-left sm:text-center text-ternary-dark dark:text-primary-light">
                            Leaders
                        </p>
                        <p className="font-general-medium text-lg md:text-xl lg:text-2xl xl:text-3xl text-gray-500 dark:text-gray-200 mr-200">
                            Not only due to our values, but it also takes <br />
                            leadership to moveour ship forward.
                        </p>
                    </section>
                    <div>
                    <img alt="image1" src={image1} style={styleProfil} />
                    <img alt="image2" src={image1} style={styleProfil2} />
                    <img alt="image3" src={image1} style={styleProfil3} />
                    </div>
                    
                </div>
            </div>
        </>
    );
};

export default OurLeaders;

const styleProfil = {
    borderRadius: "50%",
    width: "270px",
    marginLeft: "-300px",
    marginTop: "20px"
};

const styleProfil2 = {
    borderRadius: "50%",
    width: "270px",
    marginLeft: "-100px"
};

const styleProfil3 = {
    borderRadius: "50%",
    width: "270px",
    marginLeft: "-250px"
};