
const Mission = () => {
    return (
        <>
            <div className="container mx-auto">
                <section className="py-5 sm:py-10 mt-30 sm:mt-20">
                    <div className="text-center">
                        <p className="font-general-bold text-2xl lg:text-4xl xl:text-8xl text-center sm:text-center text-ternary-dark dark:text-primary-light">
                            Mission
                        </p>
                    </div>
                </section>
            </div>
        </>
    );
};

export default Mission;

