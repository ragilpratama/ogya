import useThemeSwitcher from '../../hooks/useThemeSwitcher';
import { FiArrowDownCircle } from 'react-icons/fi';
import developerLight from '../../images/developer.svg';
import developerDark from '../../images/developer-dark.svg';
import { motion } from 'framer-motion';
import Content from '../../components/shared/Content';
import OurServices from '../Services/OurServices';
import Partners from '../Services/Partners';
// import Button from '../reusable/Button';

const Jobs = () => {
    const [activeTheme] = useThemeSwitcher();

    return (
        <motion.section
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ ease: 'easeInOut', duration: 0.9, delay: 0.2 }}
            className="sm:justify-between items-center sm:flex-row mt-10 mr-10"
        >
            <div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.2,
                    }}
                    className="font-general-bold text-3xl lg:text-8xl xl:text-8xl text-right sm:text-right text-ternary-dark dark:text-primary-light mt-20 md:mt-0"
                >
                    Job Oppurtunities
                </motion.div>
                <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{
                        ease: 'easeInOut',
                        duration: 0.9,
                        delay: 0.3,
                    }}
                    className="flex justify-right float-right sm:block"
                >
                    <a
                        // download="Stoman-Resume.pdf"
                        href=""
                        className="font-general-medium flex justify-right items-center w-36 sm:w-48 mt-12 mb-6 sm:mb-0 text-lg justify-right"
                        aria-label="Download Resume"
                    >
                        <span className="block bg-blue rounded-xl text-center text-lg w-40 text-white dark:text-ternary-light hover:text-white dark:hover:text-secondary-light  sm:mx-4 mb-2 sm:py-2 sm:px-4">
                            Apply now
                        </span>
                    </a>
                </motion.div>

            </div>
            <div style={{ display: "flex" }}>
                <div className="container">
                    <section className="py-5 sm:py-10 mt-5 sm:mt-10">
                        <div className="text-center">
                            <p className="font-general-medium mt-10 text-lg md:text-xl lg:text-2xl xl:text-3xl text-left sm:text-left text-gray-500 dark:text-gray-200">
                                Job Vacancy
                            </p>
                        </div>
                        <div style={{ display: "inline-block", flexWrap: "wrap" }}>
                            <div className="flex" >
                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        16.2 %
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Return on Stockholder's Equity
                                    </p>
                                </div>

                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        $9.6B
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Total Revenues
                                    </p>
                                </div>
                            </div>
                            <div className="flex">
                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        $3.66B
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Net Income Per Diluted Share
                                    </p>
                                </div>
                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        $3.66B
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Net Income Per Diluted Share
                                    </p>
                                </div>
                            </div>
                            <div className="flex">
                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        $3.66B
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Net Income Per Diluted Share
                                    </p>
                                </div>
                                <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
                                    <p className="font-general-bold text-xl text-center">
                                        $3.66B
                                    </p>
                                    <p className="text-xl text-center mt-5">
                                        Net Income Per Diluted Share
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div className="container mx-auto">
                    <section className="py-5 sm:py-10 mt-5 sm:mt-10" style={style}>
                        <div className="text-center">
                            <p className="font-general-medium mt-10 text-lg md:text-xl lg:text-2xl xl:text-3xl text-left sm:text-left text-gray-500 dark:text-gray-200">
                                Function
                            </p>
                        </div>
                        <div style={{ display: "flex", flexWrap: "wrap" }}>
                            <button style={button}>All Function</button>
                            <button style={button}>Developers</button>
                            <button style={button}>Developers</button>
                            <button style={button}>Developers</button>
                            <button style={button}>Developers</button>
                            <button style={button}>Developers</button>
                            <button style={button}>Developers</button>
                        </div>
                    </section>
                </div>
            </div>
        </motion.section>

    );
};

export default Jobs;

const insideStyles = {
    background: "#e5e7eb52",
    width: "260px",
    padding: "30px",
    margin: "10px",
    left: "50%",
    color: "white",
};

const button = {
    background: "#ddd",
    border: "noun",
    color: "black",
    padding: "10px 20px ",
    textAlign: "center",
    textDecoration: "none",
    display: "flex",
    margin: "10px 10px",
    curson: "pointer",
    borderRadius: "16px",
    flexWrap: "wrap"
};

const style = {
    marginLeft: "30%"
};
