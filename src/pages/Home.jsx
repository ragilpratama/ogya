import AppBanner from "../components/shared/AppBanner";
import ImgPoweredBy from "../images/poweredby.png";
import { Parallax, Background } from "react-parallax";
import imageOgya from "../images/ogya-2.jpg";
import headsetIcon from "../images/headphones-icon.png";
import Content from "../components/shared/Content";

const Home = () => {
  return (
    <>
      <div className="container mx-auto">
        <AppBanner></AppBanner>
        <section className="py-5 sm:py-10 mt-5 sm:mt-10">
          <div className="text-center">
            <p className="font-general-medium text-md mb-1 text-ternary-dark dark:text-ternary-light">
              Powered By
            </p>
            <div className="flex justify-center">
              <img alt="Powered By" src={ImgPoweredBy} width={370} />
            </div>
          </div>
        </section>
      </div>
      <Parallax
        bgImage={imageOgya}
        bgImageStyle={{ objectFit: "cover" }}
        strength={200}
      >
        <div style={{ height: "100vh" }}>
          <div style={insideStyles} className="w-80p md:w-50p rounded-xl">
            <p className="font-general-bold text-xl text-center">
              Businesses had higher productivity after we helped them go digital
            </p>
            <p>
              Through digitalization, we can help you improve productivity
              through better efficiency. With years of experience, our team has
              the confidence to help your businnes thrive.
            </p>
          </div>
        </div>
      </Parallax>
      <div className="container mx-auto">
        <Content />
      </div>
      <a style={fixedButton} href>
        <div style={roundedFixedBtn}>
          <img src={headsetIcon} alt="contact us" width={35} />
        </div>
      </a>
    </>
  );
};

export default Home;

const insideStyles = {
  background: "rgba(255, 255, 255, .7)",
  padding: 20,
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
};

const fixedButton = {
  position: "fixed",
  bottom: "0px",
  left: "0px",
  padding: "20px",
};
const roundedFixedBtn = {
  height: "60px",
  lineHeight: "80px",
  width: "60px",
  borderRadius: "50%",
  backgroundColor: "#7ebb52",
  color: "white",
  textAlign: "center",
  cursor: "pointer",
  paddingLeft: "13px",
  paddingTop: "8px",
};
