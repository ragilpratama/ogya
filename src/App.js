import { AnimatePresence } from 'framer-motion';
import { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop';
import AppFooter from './components/shared/AppFooter';
import AppHeader from './components/shared/AppHeader';
import './css/App.css';
import UseScrollToTop from './hooks/useScrollToTop';

const AboutUs = lazy(() => import('../src/pages/AboutUs/AboutUs'));
const Contact = lazy(() => import('./pages/Contact.jsx'));
const Home = lazy(() => import('./pages/Home'));
const Projects = lazy(() => import('./pages/Projects'));
const ProjectSingle = lazy(() => import('./pages/ProjectSingle.jsx'));
const Vision = lazy(() => import('./pages/AboutUs/Vision'));
const Mission = lazy(() => import('./pages/AboutUs/Mission'));
const Values = lazy(() => import('./pages/AboutUs/OurValues'));
const OurLeaders = lazy(() => import('./pages/AboutUs/OurLeaders'));
const Services = lazy(() => import('./pages/Services/Services'));
const OurServices = lazy(() => import('./pages/Services/OurServices'));
const Partners = lazy(() => import('./pages/Services/Partners'));
const Investor = lazy(() => import('./pages/Investor/Investor'));
const FinancialReports = lazy(() => import('./pages/Investor/FinancialReport'));
const Jobs = lazy(() => import('./pages/Careers/Jobs'));

function App() {
	return (
		<AnimatePresence>
			<div className=" bg-secondary-light dark:bg-primary-dark transition duration-300">
				<Router>
					<ScrollToTop />
					<AppHeader />
					<Suspense fallback={""}>
						<Routes>
							<Route path="/" element={<Home />} />
							<Route path="projects" element={<Projects />} />
							<Route
								path="projects/single-project"
								element={<ProjectSingle />}
							/>

							<Route path="about" element={<AboutUs />} />
							<Route path="ourLeaders" element={<OurLeaders />} />
							<Route path="vision" element={<Vision />} />
							<Route path="mission" element={<Mission />} />
							<Route path="values" element={<Values />} />
							<Route path="service" element={<Services />} />
							<Route path="ourServices" element={<OurServices />} />
							<Route path="partners" element={<Partners />} />
							<Route path="investor" element={<Investor />} />
							<Route path="financialReports" element={<FinancialReports />} />
							<Route path="jobs" element={<Jobs />} />
						</Routes>
					</Suspense>
					<AppFooter />
				</Router>
				<UseScrollToTop />
			</div>
		</AnimatePresence>
	);
}

export default App;
